const ship = ((player,length) => {
  let isSunk = false;
  let image1 = "player1";
  let image2 = "player2";
  const image = player ? image1 : image2;
  const getLength = () => {return length;};
  const generateWhereHitObject = length => {
    let generatedObject = {};
    for(let i=1; i<length+1; i++) {
      generatedObject[i] = false;
    };
    return generatedObject;
  };

  let whereHit = generateWhereHitObject(length);
  const getWhereHit = () => {return whereHit;};
  const hit = placeHit => {
    let returnValue = null;
    whereHit[placeHit] ? returnValue = "Already hit here" : whereHit[placeHit] = true;
    return returnValue;
  };
  const hasItSunk = () => {
    for(let i=1;i<Object.keys(whereHit).length+1;i++)
    {
      if(!whereHit[i])
      {
        return false;
      }
    }
    isSunk=true;
    return true;
  };
  return {hit, hasItSunk,getWhereHit,getLength};
})
module.exports = ship;
