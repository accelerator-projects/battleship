const Gameboard = require('./Gameboard');
const Ship = require('./Ship');
const Ai = require('./Ai');

describe('Gameboard', function () {

  it('should be an object', function () {
    let gameStart=true;
    const aiBoard = Gameboard();
    const opponentBoard = Gameboard();
    const ai = Ai(2,gameStart,aiBoard,opponentBoard);
    expect(typeof ai).toBe('object');
  });
  it('places a ship in a random place on the board',function () {
    let gameStart=true;
    const aiBoard = Gameboard();
    const opponentBoard = Gameboard();
    const ai = Ai(2,gameStart,aiBoard,opponentBoard);
    ai.placeShip();
    let isShipPlaced = false;
    for(let i=0;i<10;i++)
    {
      for(let j=0;j<10;j++)
      {
        if(!(aiBoard.getBoard()[String(i)+String(j)]===null))
        {
          isShipPlaced=true;
          break;
        }
      }
      if(isShipPlaced)
      {
        break;
      }
    }
    expect(isShipPlaced).toEqual(true);
  })
  it('performs an attack according to a random strategy', function () {
    let gameStart=true;
    const aiBoard = Gameboard();
    const opponentBoard = Gameboard();
    const ai = Ai(2,gameStart,aiBoard,opponentBoard);
    ai.attack();
    let hasAttacked = false;
    for(let i=0;i<10;i++)
    {
      for(let j=0;j<10;j++)
      {
        if(!(opponentBoard.getBoard()[String(i)+String(j)]===null))
        {
          hasAttacked=true;
          break;
        }
      }
      if(hasAttacked)
      {
        break;
      }
    }
    expect(hasAttacked).toEqual(true);
    numberOfMisses = 0;
    ai.attack();
    for(let i=0;i<10;i++)
    {
      for(let j=0;j<10;j++)
      {
        if(opponentBoard.getBoard()[String(i)+String(j)]==='miss')
        {
          numberOfMisses++;
        }
      }
    }
    expect(numberOfMisses).toEqual(2);
  })
})
