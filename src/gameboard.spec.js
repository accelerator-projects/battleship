const Gameboard = require('./Gameboard');
const Ship = require('./Ship');

describe('Gameboard', function () {

  it('should be an object', function () {
    const gameboard = Gameboard();
    expect(typeof gameboard).toBe('object');
  });

  it('places ship on board', function () {
    const ship = Ship(1,3);
    const gameboard = Gameboard();
    gameboard.placeShip(ship,3,3);
    expect(gameboard.getBoard()['33'].ship).toEqual(ship);
    expect(gameboard.getBoard()['33'].position).toEqual('1');
    expect(gameboard.getBoard()['43'].ship).toEqual(ship);
    expect(gameboard.getBoard()['43'].position).toEqual('2');
    expect(gameboard.getBoard()['53'].ship).toEqual(ship);
    expect(gameboard.getBoard()['53'].position).toEqual('3');
  });
  it('places a ship vertically on board', function () {
    const ship = Ship(1,3);
    const gameboard = Gameboard();
    gameboard.placeShipVertical(ship,3,3);
    expect(gameboard.getBoard()['33'].ship).toEqual(ship);
    expect(gameboard.getBoard()['33'].position).toEqual('1');
    expect(gameboard.getBoard()['34'].ship).toEqual(ship);
    expect(gameboard.getBoard()['34'].position).toEqual('2');
    expect(gameboard.getBoard()['35'].ship).toEqual(ship);
    expect(gameboard.getBoard()['35'].position).toEqual('3');
  })
  it("doesn't let you place a ship outside the board", function () {
    const ship = Ship(1,4);
    const gameboard = Gameboard();
    expect(gameboard.placeShip(ship,11,11)).toEqual("You can't place a ship here");
    expect(gameboard.placeShip(ship,9,9)).toEqual("You can't place a ship here");
  });
  it("rotates a ship", function () {
    const ship = Ship(1,3);
    const gameboard = Gameboard();
    gameboard.placeShip(ship,3,3);
    gameboard.placeShipVertical(ship,3,3,true);
    expect(gameboard.getBoard()['43']).toEqual(null);
    expect(gameboard.getBoard()['53']).toEqual(null);
    expect(gameboard.getBoard()['33'].ship).toEqual(ship);
    expect(gameboard.getBoard()['33'].position).toEqual('1');
    expect(gameboard.getBoard()['34'].ship).toEqual(ship);
    expect(gameboard.getBoard()['34'].position).toEqual('2');
    expect(gameboard.getBoard()['35'].ship).toEqual(ship);
    expect(gameboard.getBoard()['35'].position).toEqual('3');
  });
  it("moves a ship to a different position", function () {
    const ship = Ship(1,3);
    const gameboard = Gameboard();
    gameboard.placeShip(ship,3,3);
    gameboard.placeShip(ship,6,3,true,3,3);
    expect(gameboard.getBoard()['43']).toEqual(null);
    expect(gameboard.getBoard()['53']).toEqual(null);
    expect(gameboard.getBoard()['33']).toEqual(null);
    expect(gameboard.getBoard()['63'].ship).toEqual(ship);
    expect(gameboard.getBoard()['63'].position).toEqual('1');
    expect(gameboard.getBoard()['73'].ship).toEqual(ship);
    expect(gameboard.getBoard()['73'].position).toEqual('2');
    expect(gameboard.getBoard()['83'].ship).toEqual(ship);
    expect(gameboard.getBoard()['83'].position).toEqual('3');
  })
  it("allows a ship to be hit in the correct spot", function () {
    const ship = Ship(1,4);
    const gameboard = Gameboard();
    gameboard.placeShip(ship,3,3);
    gameboard.receiveAttack(4,3);
    expect(gameboard.getBoard()['43'].ship.getWhereHit()['2']).toEqual(true);
  });
  it("records a missed attack", function () {
    const ship = Ship(1,4);
    const gameboard = Gameboard();
    gameboard.placeShip(ship,3,3);
    gameboard.receiveAttack(4,4);
    expect(gameboard.getBoard()['44']).toEqual('miss');
  });
  it("doesn't let you attack the same spot twice", function () {
    const ship = Ship(1,4);
    const gameboard = Gameboard();
    gameboard.placeShip(ship,3,3);
    gameboard.receiveAttack(4,4);
    gameboard.receiveAttack(4,3);
    expect(gameboard.receiveAttack(4,3)).toEqual('You have already attacked here.');
    expect(gameboard.receiveAttack(4,4)).toEqual('You have already attacked here.');
  });
});
