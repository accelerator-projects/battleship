const Gameboard = require('./Gameboard');
const Ship = require('./Ship');
const Player = require('./Player');

const ai =((playerNum,gamestart,aiBoard,opponentBoard) => {
  let ships = [Ship(2,2),Ship(2,3),Ship(2,3),Ship(2,4),Ship(2,5)]
  const generateTopToBottom = () => {
    let generatedArray = [];
    for(let i=0;i<10;i++)
    {
      for(let j=0;j<10;j++)
      {
        generatedArray.push({x:i,y:j});
      }
    }
    return generatedArray;
  }
  const generateLeftToRight = () => {
    let generatedArray = [];
    for(let i=0;i<10;i++)
    {
      for(let j=0;j<10;j++)
      {
        generatedArray.push({x:j,y:i});
      }
    }
    return generatedArray;
  }
  const leftToRightAttack = () => {
    return leftToRight.shift();
  }
  const rightToLeftAttack = () => {
    return leftToRight.pop();
  }
  const topToBottomAttack = () => {
    return topToBottom.shift();
  }
  const bottomToTopAttack = () => {
    return topToBottom.pop();
  }
  let topToBottom = generateTopToBottom();
  let leftToRight = generateLeftToRight();
  let strategies = [leftToRightAttack,rightToLeftAttack,topToBottomAttack,bottomToTopAttack];
  let chosenStrategy=strategies[Math.floor(Math.random() * 4)];
    const placeShip = () => {
      const ship = ships.pop();
      let attemptedShipPlacement = "You can't place a ship here";
      while(attemptedShipPlacement=="You can't place a ship here"){
        if(Math.random()>=0.5)
        {
          attemptedShipPlacement=aiBoard.placeShip(ship,Math.floor((Math.random() * 10)),Math.floor((Math.random() * 10)));
        }
        else {
          attemptedShipPlacement=aiBoard.placeShipVertical(ship,Math.floor((Math.random() * 10)),Math.floor((Math.random() * 10)));
        }
      };
    }

    const attack = () => {
      let chosenStrategyObject=chosenStrategy();
      return [chosenStrategyObject,opponentBoard.receiveAttack(chosenStrategyObject.x,chosenStrategyObject.y)];
    }
    return {placeShip,attack};
});
module.exports = ai;
