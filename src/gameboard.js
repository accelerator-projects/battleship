const gameboard = (() => {
  let sunkShips = 0;
  const generateBoard = () => {
    let board = {};
    for(let i=0;i<10;i++)
    {
      for(let j=0;j<10;j++)
      {
        board[String(i)+String(j)] = null;
      }
    }
    return board;
  }
  const board = generateBoard();
  const placeShip = (ship,x,y,isRotate,oldX,oldY) => {
    if(isRotate)
    {
      for(let i=0;i<ship.getLength();i++)
      {
        board[String(x)+String(y+i)] = null;
      }
    }
    if(oldX)
    {
      for(let i=0;i<ship.getLength();i++)
      {
        board[String(oldX+i)+String(oldY)] = null;
      }
    }
    for(let i=0;i<ship.getLength();i++)
    {
      if(board[String(x+i)+String(y)] || board[String(x+i-1)+String(y)] || board[String(x+i)+String(y-1)] || board[String(x+i+1)+String(y)] || board[String(x+i)+String(y+1)] ||
    x+i<0 || x+i>9 || y<0 || y>9)
      {
        return "You can't place a ship here";
      }
    }
    for(let i=0;i<ship.getLength();i++)
    {
        board[String(x+i)+String(y)] = {ship:ship,position:String(i+1)};
    }
  }
  const placeShipVertical = (ship,x,y,oldX,oldY) => {
    for(let i=0;i<ship.getLength();i++)
    {
      board[String(x+i)+String(y)] = null;
    }
    if(oldX)
    {
      for(let i=0;i<ship.getLength();i++)
      {
        board[String(oldX)+String(oldY+i)] = null;
      }
    }
    for(let i=0;i<ship.getLength();i++)
    {
      if(board[String(x)+String(y+i)] || board[String(x)+String(y+i-1)] || board[String(x-1)+String(y+i)] || board[String(x+1)+String(y+i)] || board[String(x)+String(y+1+i)] ||
    x<0 || x>9 || y+i<0 || y+i>9)
      {
        return "You can't place a ship here";
      }
    }
    for(let i=0;i<ship.getLength();i++)
    {

        board[String(x)+String(y+i)] = {ship:ship,position:String(i+1)};
    }
  }
  const getBoard = () => {return board;};
  const receiveAttack = (x,y) => {
    if(!(board[String(x)+String(y)] === null) && board[String(x)+String(y)]!="miss")
    {
      if(board[String(x)+String(y)].ship.getWhereHit()[board[String(x)+String(y)].position])
      {
        return "You have already attacked here.";
      }
      board[String(x)+String(y)].ship.hit(board[String(x)+String(y)].position);
      if(board[String(x)+String(y)].ship.hasItSunk())
      {
        sunkShips++;
        return "Ship sunk";
      }
      return "hit";
    }
    if(board[String(x)+String(y)]=='miss')
    {
      return "You have already attacked here.";
    }
    board[String(x)+String(y)]='miss';
    return "miss";
  }
  const areAllShipsSunk = () => {
    if(sunkShips==5)
    {
      return true;
    }
    return false;
  }
  return {placeShip,getBoard,receiveAttack,placeShipVertical};
});
module.exports = gameboard;
