const Gameboard = require('./Gameboard');
const Ship = require('./Ship');
const Player = require('./Player');

describe('Gameboard', function () {

  it('should be an object', function () {
    let gameStart=true;
    const playerBoard = Gameboard();
    const opponentBoard = Gameboard();
    const player = Player(1,gameStart,playerBoard,opponentBoard);
    expect(typeof player).toBe('object');
  });
  it('places a ship on the board', function () {
    let gameStart=true;
    const playerBoard = Gameboard();
    const opponentBoard = Gameboard();
    const player = Player(1,gameStart,playerBoard,opponentBoard);
    player.placeShip(4,3,3);
    expect(playerBoard.getBoard()['33'].position).toEqual('1');
    expect(playerBoard.getBoard()['43'].position).toEqual('2');
  });
  it('places a ship vertically on the board', function () {
    let gameStart=true;
    const playerBoard = Gameboard();
    const opponentBoard = Gameboard();
    const player = Player(1,gameStart,playerBoard,opponentBoard);
    player.placeShipVertical(4,3,3);
    expect(playerBoard.getBoard()['33'].position).toEqual('1');
    expect(playerBoard.getBoard()['34'].position).toEqual('2');
  })
  it('can attack a square', function () {
    let gameStart=true;
    const playerBoard = Gameboard();
    const opponentBoard = Gameboard();
    const player = Player(1,gameStart,playerBoard,opponentBoard);
    player.attack(3,3);
    expect(opponentBoard.getBoard()['33']).toEqual('miss');
  })
  it('only allows one piece of each length (except for 3 then it allows 2)', function () {
    let gameStart=true;
    const playerBoard = Gameboard();
    const opponentBoard = Gameboard();
    const player = Player(1,gameStart,playerBoard,opponentBoard);
    player.placeShip(4,3,3);
    expect(player.placeShip(4,3,5)).toEqual("Please pick a different piece");
    player.placeShip(3,3,5);
    player.placeShip(3,3,7);
    expect(playerBoard.getBoard()['37'].position).toEqual('1');
    expect(player.placeShip(3,3,9)).toEqual("Please pick a different piece");
  })
})
